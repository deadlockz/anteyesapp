package de.digisocken.anteyesapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_ANYCOLOR;
import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_ANYDEPTH;
import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_COLOR;
import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED;
import static org.opencv.imgproc.Imgproc.CV_RGBA2mRGBA;
import static org.opencv.imgproc.Imgproc.CV_mRGBA2RGBA;
import static org.opencv.imgproc.Imgproc.INTER_AREA;

//import org.bytedeco.javacv.FFmpegFrameRecorder;
//import org.bytedeco.javacv.FrameRecorder.Exception;
//import org.bytedeco.javacv.cpp.opencv_core.IplImage;


public class FdActivity extends AppCompatActivity implements CvCameraViewListener2 {
    private ActionBar ab = null;

    private static final String    TAG                 = "OCVSample::Activity";
    private static final Scalar    FACE_RECT_COLOR     = new Scalar(0, 255, 0, 255);
    public static final int        JAVA_DETECTOR       = 1;

    private Mat                    eyeImg;
    private Mat                    mRgba;
    private Mat                    mGray;
    private File                   mCascadeFile;
    private File                   mCascadeFileEye;
    private CascadeClassifier      mJavaDetector;
    private CascadeClassifier      mJavaDetectorEye;

    private String[]               mDetectorName;

    private float                  mRelativeFaceSize   = 0.2f;
    private int                    mAbsoluteFaceSize   = 0;

    private CameraBridgeViewBase   mOpenCvCameraView;

    private boolean monster = false;

/*
    private int resolutionIndex = 0;
    private IplImage videoImage = null;
    boolean recording = false;
    private volatile FFmpegFrameRecorder recorder;
    private int sampleAudioRateInHz = 44100;
    private int imageWidth = 320;
    private int imageHeight = 240;
    private int frameRate = 30;
    private Thread audioThread;
    volatile boolean runAudioThread = true;
    private AudioRecord audioRecord;
    private AudioRecordRunnable audioRecordRunnable;
    private String ffmpeg_link;
*/

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    //initRecorder();

                    try {
                        //eyeImg = Utils.loadResource(FdActivity.this, R.drawable.ani1, CV_LOAD_IMAGE_ANYCOLOR);
                        eyeImg = Utils.loadResource(FdActivity.this, R.drawable.icon, CV_LOAD_IMAGE_UNCHANGED);
                        //eyeImg.convertTo(eyeImg, CV_RGBA2mRGBA);
                        //Imgproc.cvtColor(unconverted, eyeImg, CV_mRGBA2RGBA);
                        // ANDROID_BITMAP_FORMAT_RGBA_8888
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Load native library after(!) OpenCV initialization
                    //System.loadLibrary("detection_based_tracker");

                    try {
                        // load cascade file from application resources
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);

                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mJavaDetector.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetector = null;
                        } else {
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());
                        }

                        is = getResources().openRawResource(R.raw.haarcascade_eye_tree_eyeglasses);
                        mCascadeFileEye = new File(cascadeDir, "haarcascade_eye_tree_eyeglasses.xml");
                        os = new FileOutputStream(mCascadeFileEye);

                        buffer = new byte[4096];
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetectorEye = new CascadeClassifier(mCascadeFileEye.getAbsolutePath());
                        if (mJavaDetectorEye.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetectorEye = null;
                        } else {
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFileEye.getAbsolutePath());
                        }



                        //mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }

                    mOpenCvCameraView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_FRONT);
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    /*
    private void initRecorder() {
        Log.w(TAG,"initRecorder");

        int depth = org.bytedeco.javacv.cpp.opencv_core.IPL_DEPTH_8U;
        int channels = 4;

        // if (yuvIplimage == null) {
        // Recreated after frame size is set in surface change method
        videoImage = IplImage.create(imageWidth, imageHeight, depth, channels);
        //yuvIplimage = IplImage.create(imageWidth, imageHeight, IPL_DEPTH_32S, 2);

        Log.v(TAG, "IplImage.create");
        // }

        File videoFile = new File(getExternalFilesDir(null), "VideoTest/images/video.mp4");
        boolean mk = videoFile.getParentFile().mkdirs();
        Log.v(TAG, "Mkdir: " + mk);

        boolean del = videoFile.delete();
        Log.v(TAG, "del: " + del);

        try {
            boolean created = videoFile.createNewFile();
            Log.v(TAG, "Created: " + created);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        ffmpeg_link = videoFile.getAbsolutePath();
        recorder = new FFmpegFrameRecorder(ffmpeg_link, imageWidth, imageHeight, 1);
        Log.v(TAG, "FFmpegFrameRecorder: " + ffmpeg_link + " imageWidth: " + imageWidth + " imageHeight " + imageHeight);

        recorder.setFormat("mp4");
        Log.v(TAG, "recorder.setFormat(\"mp4\")");

        recorder.setSampleRate(sampleAudioRateInHz);
        Log.v(TAG, "recorder.setSampleRate(sampleAudioRateInHz)");

        // re-set in the surface changed method as well
        recorder.setFrameRate(frameRate);
        Log.v(TAG, "recorder.setFrameRate(frameRate)");

        // Create audio recording thread
        audioRecordRunnable = new AudioRecordRunnable();
        audioThread = new Thread(audioRecordRunnable);
    }
    */

    public FdActivity() {
        mDetectorName = new String[2];
        mDetectorName[JAVA_DETECTOR] = "Java";
        //mDetectorName[NATIVE_DETECTOR] = "Native (tracking)";

        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.face_detect_surface_view);

        try {
            ab = getSupportActionBar();
            if (ab != null) {
                ab.setDisplayShowHomeEnabled(true);
                ab.setHomeButtonEnabled(true);
                ab.setDisplayUseLogoEnabled(true);
                ab.setLogo(R.drawable.icon);
                ab.setTitle("  " + getString(R.string.app_name));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_ant) {
            monster = false;
            return true;
        } else if (id == R.id.action_cookiemonster) {
            monster = true;
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();
        //Core.rotate(mRgba2, mRgba, Core.ROTATE_90_CLOCKWISE);
        //Core.rotate(mGray2, mGray, Core.ROTATE_90_CLOCKWISE);

        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
            //mNativeDetector.setMinFaceSize(mAbsoluteFaceSize);
        }

        MatOfRect faces = new MatOfRect();


        if (mJavaDetector != null) {
                mJavaDetector.detectMultiScale(
                        mGray,
                        faces,
                        1.1,
                        2,
                        2,
                        new Size(mAbsoluteFaceSize, mAbsoluteFaceSize),
                        new Size()
                );
        } else {
            Log.e(TAG, "Detection method is not selected!");
        }

        Rect[] facesArray = faces.toArray();

        for (int i = 0; i < facesArray.length; i++) {
            //Imgproc.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);

            MatOfRect eyes = new MatOfRect();

            mJavaDetectorEye.detectMultiScale(
                    new Mat(mGray, facesArray[i]),
                    eyes,
                    1.1,
                    2,
                    2,
                    new Size(25, 25),
                    new Size()
            );

            Rect[] eyesArray = eyes.toArray();
            if (eyesArray.length != 2) continue;

            for( int j = 0; j < eyesArray.length; j++) {

                if (monster) {
                    Point center = new Point(
                            facesArray[i].x + eyesArray[j].x + eyesArray[j].width*0.5,
                            facesArray[i].y + eyesArray[j].y + eyesArray[j].height*0.5
                    );
                    Point center2 = center.clone();
                    center2.x += Math.random() * 6 -3;
                    center2.y += Math.random() * 6 -3;

                    int radius = (int) Math.round( (eyesArray[j].width + eyesArray[j].height)*0.125 );
                    Imgproc.circle( mRgba, center, radius*2, new Scalar( 255, 255, 255 ), -1, 4, 0 );
                    Imgproc.circle( mRgba, center2, radius, new Scalar( 0, 0, 0 ), -1, 4, 0 );

                } else {
                    Mat resizedimage = new Mat();
                    Size scaleSize = new Size(eyesArray[j].width, eyesArray[j].height);
                    Imgproc.resize(eyeImg, resizedimage, scaleSize, 0, 0, INTER_AREA);

                    resizedimage.copyTo(
                            mRgba.rowRange(facesArray[i].y + eyesArray[j].y, facesArray[i].y + eyesArray[j].y + resizedimage.rows())
                                    .colRange(facesArray[i].x + eyesArray[j].x, facesArray[i].x + eyesArray[j].x + resizedimage.cols())
                    );
                }

            }

        }

        return mRgba;
    }

}
